@demoblazetest
Feature: Test Demoblaze "Add to cart", "Delete", "Place order" and "Purchase" functionality.
Description: The Purpose of this test is to test Demoblaze online shop end to end functionality.

Scenario: Customer navigates through all product categories and purchases Laptop.
Given user is on Demoblaze Online shop home page
And user navigates through "Phones" category
And user navigates through "Laptops" category
And user navigates through "Monitors" category
And user navigates through "Laptops" category
And adds "Sony vaio i5" to Cart
And user navigates through "Laptops" category
And adds "Dell i7 8gb" to Cart
And Deletes "Dell i7 8gb" from Cart
Then "Dell i7 8gb" should be deleted from Cart
And user places order and fill required personal and fincancial details 
|  Name  | Country |   City  | Credit Card | Month  | Year |
| Ashish  | India   |Gurugram | 6568******* |  12    | 2022 |
And proceeds to purchase products in cart
Then purchase amount should be "790 USD"	