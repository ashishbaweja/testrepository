package cucumberautomation.demoblazetest.pageObjects;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.DataTable;
import cucumberautomation.demoblazetest.utility.BaseTest;

public class PlaceOrderPage extends BaseTest{

    Logger Log = Logger.getLogger("PlaceOrderPage");
	
	@FindBy(xpath="//button[text()=\"Place Order\"]")
	WebElement btn_Place_Order;
	@FindBy(id="name")
	WebElement txt_Name;
	@FindBy(id="country")
	WebElement txt_Country;
	@FindBy(id="city")
	WebElement txt_City;
	@FindBy(id="card")
	WebElement txt_Card;
	@FindBy(id="month")
	WebElement txt_Month;
	@FindBy(id="year")
	WebElement txt_Year;
	@FindBy(xpath="//button[text()=\"Purchase\"]")
	WebElement btn_Purchase;
	@FindBy(xpath="//p[contains(.,'Amount')]")
	WebElement elm_Amount;
	@FindBy(xpath="//button[text()=\"OK\"]")
	WebElement btn_OK;
	
	private WebDriver driver;

	public PlaceOrderPage(WebDriver driver)
	{
    this.driver=driver;
	PageFactory.initElements(driver, this);	
	}

	public void PlaceOrder(DataTable userdata) 
	{
		Logger Log = Logger.getLogger("PlaceOrderPage");
		btn_Place_Order.click();
		Log.info("User placing order now");
		waituntilelementisvisible(driver,txt_Name,5);
		for(java.util.Map<String,String> data : userdata.asMaps(String.class, String.class))
		{
			txt_Name.sendKeys(data.get("Name")); 
		    Log.info("Name Entered");
			txt_Country.sendKeys(data.get("Country")); 
		    Log.info("Country Entered");
			txt_City.sendKeys(data.get("City")); 
		    Log.info("City Entered");
			txt_Card.sendKeys(data.get("Credit Card")); 
		    Log.info("Name Entered");
		    txt_Month.sendKeys(data.get("Month")); 
		    Log.info("Month Entered");
			txt_Year.sendKeys(data.get("Year")); 
		    Log.info("Year Entered");
		
		}
	}
	
	public void purchaseProduct() 
	{
		  btn_Purchase.click();
		  Log.info("Laptop purchased successfully");
	}
	
	
	public void purcahseAmountshouldbe(String ExpectedAmount) throws IOException
	{
		 Getscreenshot(driver,"OrderSnapshot");
		 String Amount=elm_Amount.getText();
		 System.out.println(Amount);
		 String pattern ="Amount:\\s*([^\\n]+)";
		 
		 Pattern p = Pattern.compile(pattern);
		 Matcher m = p.matcher(Amount);
		 while (m.find())
		 { 
	            Assert.assertEquals(ExpectedAmount,m.group(1).trim());
	     } 

		 btn_OK.click();
	}

	
	
	
	
}
