package cucumberautomation.demoblazetest.pageObjects;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import cucumberautomation.demoblazetest.utility.BaseTest;

public class HomePage extends BaseTest {
	
	private WebDriver driver;
	Properties prop;
	private String HomeURL;
	Logger Log = Logger.getLogger("HomePage");


	
	@FindBy(xpath="//a[@class=\"navbar-brand\"]")
	WebElement Elm_Title;
	@FindBy(xpath="//*[text()=\"Home \"]")
	WebElement lnk_Homepage;
	@FindBy(xpath="//*[text()=\"Phones\"]")
	WebElement lnk_Phones;
	@FindBy(xpath="//*[text()=\"Laptops\"]")
	WebElement lnk_Laptops;
	@FindBy(xpath="//*[text()=\"Monitors\"]")
	WebElement lnk_Monitors;
	@FindBy(xpath="//*[@id=\"tbodyid\"]/div")
	WebElement Products;
	
	public HomePage(WebDriver driver)
	{
	this.driver=driver;
	PageFactory.initElements(driver, this);
	}
	
	
	public void NavigateToHomePage() throws IOException 
	{	DOMConfigurator.configure("log4j.xml");
		prop = ReadConfigData();
	    HomeURL=prop.getProperty("homepageurl");
		driver.get(HomeURL);
		
	    Log.info("User is on homepage");
		waituntilelementisvisible(driver,Elm_Title,5);		
	}


	public void browseThroughSelectedCategory(String category) {
	if(category.equals("Phones"))
	{   
		waituntilelementisvisible(driver,lnk_Phones,5);
		lnk_Phones.click();
		waituntilelementisvisible(driver,Products,5);
	    Log.info("User Browsing through Phones ");
	}
	else if(category.equals("Laptops"))
	{
		lnk_Homepage.click();
		waituntilelementisvisible(driver,lnk_Laptops,5);
		lnk_Laptops.click();
		waituntilelementisvisible(driver,Products,5);
		Log.info("User Browsing through Laptops ");
	}
	else if(category.equals("Monitors"))
	{
		waituntilelementisvisible(driver,lnk_Monitors,5);
		lnk_Monitors.click();
		waituntilelementisvisible(driver,Products,5);
		Log.info("User Browsing through Monitors ");
	}
		
	}

	
}
