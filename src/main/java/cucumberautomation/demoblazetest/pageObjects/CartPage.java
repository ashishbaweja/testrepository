package cucumberautomation.demoblazetest.pageObjects;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumberautomation.demoblazetest.utility.BaseTest;

public class CartPage extends BaseTest{
	Logger Log = Logger.getLogger("CartPage");
	
	@FindBy(xpath="//a[text()=\"Cart\"]")
	WebElement lnk_Cart;
	@FindBy(xpath="//td[text()=\"Dell i7 8gb\"]//following-sibling::td[2]/a")
	WebElement link_Delete_Dell;
	@FindAll(@FindBy(xpath="//td[text()=\"Dell i7 8gb\"]//following-sibling::td[2]/a"))
	List<WebElement> list_Delete_Dell;
	
	
	private WebDriver driver;

	public CartPage(WebDriver driver)
	{
    this.driver=driver;
	PageFactory.initElements(driver, this);	
	}

	public void NavigateToCart() 
	{
		Logger Log = Logger.getLogger("HomePage");
		lnk_Cart.click();
		Log.info("User navigated to cart");
	}
	public void DeleteProductFromCart(String product) throws InterruptedException 
	{
		if(product.contentEquals("Dell i7 8gb"))
		{
			waituntilelementisvisible(driver,link_Delete_Dell,5);
			link_Delete_Dell.click();
			Log.info("Deleting " +product +" from cart");
			Thread.sleep(4000);
			
		}
	}	
	public void verifyProductIsDeleted(String product)	
	{
	    Assert.assertEquals(0,list_Delete_Dell.size());
		Log.info(product + " deleted from cart");
		}
	}
		
	
	
	
	

