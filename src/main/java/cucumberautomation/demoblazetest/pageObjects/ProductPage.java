package cucumberautomation.demoblazetest.pageObjects;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import cucumberautomation.demoblazetest.utility.BaseTest;

public class ProductPage extends BaseTest {
	
	Logger Log = Logger.getLogger("ProductPage");
	
	@FindBy(xpath="//a[text()=\"Sony vaio i5\"]")
	WebElement lnk_SonyVaioi5;
	@FindBy(xpath="//a[text()=\"Dell i7 8gb\"]")
	WebElement lnk_Delli7;
	@FindBy(xpath="//a[text()=\"Add to cart\"]")
	WebElement btn_AddToCart;
	
	private WebDriver driver;

	public ProductPage(WebDriver driver)
	{
    this.driver=driver;
	PageFactory.initElements(driver, this);	
	}
	
	public void addProductToCart(String product) throws InterruptedException
	{	DOMConfigurator.configure("log4j.xml");
		
	if (product.equals("Sony vaio i5"))
	{
		waituntilelementisvisible(driver,lnk_SonyVaioi5,5);	
		boolean staleElement = true; 
		while(staleElement){
		  try{
			  lnk_SonyVaioi5.click();
		       staleElement = false;
		  } catch(StaleElementReferenceException e){
		       staleElement = true;
		  }
		}				
		
	}
	else if (product.equals("Dell i7 8gb"))
	{
		waituntilelementisvisible(driver,lnk_Delli7,5);	
		boolean staleElement = true; 
		while(staleElement){
		  try{
			   lnk_Delli7.click();
		       staleElement = false;
		  } catch(StaleElementReferenceException e){
		       staleElement = true;
		  }
		}				
		
	}
	btn_AddToCart.click();
	Thread.sleep(2000);
	driver.switchTo().alert().accept();
	Log.info(product + " added to cart");
	}
}

