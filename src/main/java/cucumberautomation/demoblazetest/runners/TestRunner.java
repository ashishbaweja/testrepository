package cucumberautomation.demoblazetest.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(
	 features = "src/test/java/cucumberautomation/demoblazetest/functionaltests",
	 glue= {"cucumberautomation.demoblazetest.stepDefinitions"},
	 plugin = {"html:target/site/cucumber-pretty","json:target/cucumber.json"},
	 tags= {"@demoblazetest"},
	 monochrome = true,
	 strict = true
 )
public class TestRunner {
}