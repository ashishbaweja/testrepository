package cucumberautomation.demoblazetest.stepDefinitions;


import java.io.IOException;
import org.openqa.selenium.WebDriver;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberautomation.demoblazetest.managers.PageObjectManager;
import cucumberautomation.demoblazetest.pageObjects.CartPage;
import cucumberautomation.demoblazetest.pageObjects.HomePage;
import cucumberautomation.demoblazetest.pageObjects.PlaceOrderPage;
import cucumberautomation.demoblazetest.pageObjects.ProductPage;
import cucumberautomation.demoblazetest.utility.BaseTest;


public class DemoblazeSteps extends BaseTest {
	
	private static WebDriver driver;
	private HomePage homepage ;
	private ProductPage productpage;
	private CartPage cartpage;
	private PlaceOrderPage placeorderpage;
	private PageObjectManager pageobjectmanager;
	
@Before
public void suitesetup() throws IOException
{
	driver=setupBrowser();	
	pageobjectmanager= new PageObjectManager(driver);
}
	

@Given("^user is on Demoblaze Online shop home page$")
public void user_is_on_Demoblaze_Online_shop_home_page() throws IOException  {
	
	
	homepage=pageobjectmanager.getHomePage();
	homepage.NavigateToHomePage();
 
}

@When("^user navigates through \"([^\"]*)\" category$")
public void user_navigates_through_category(String category) {
 homepage.browseThroughSelectedCategory(category);
}


@Given("^adds \"([^\"]*)\" to Cart$")
public void adds_to_Cart(String product) throws InterruptedException {
	productpage=pageobjectmanager.getProductPage();
    productpage.addProductToCart(product);

}

@Given("^Deletes \"([^\"]*)\" from Cart$")
public void deletes_from_Cart(String product) throws InterruptedException {
	cartpage=pageobjectmanager.getCartPage();
	cartpage.NavigateToCart();
	cartpage.DeleteProductFromCart(product);
 
}

@Then("^\"([^\"]*)\" should be deleted from Cart$")
public void should_be_deleted_from_Cart(String product){
	cartpage.verifyProductIsDeleted(product);

}

@Then("^user places order and fill required personal and fincancial details$")
public void user_places_order_and_fill_required_personal_and_fincancial_details(DataTable userdata) {
	placeorderpage=pageobjectmanager.getPlaceOrderPage();
	placeorderpage.PlaceOrder(userdata);
	
}


@Then("^proceeds to purchase products in cart$")
public void proceeds_to_purchase_products_in_cart()  {

	placeorderpage.purchaseProduct();
}

@Then("^purchase amount should be \"([^\"]*)\"$")
public void purchase_amount_should_be(String amount) throws IOException 
{
	placeorderpage.purcahseAmountshouldbe(amount);
 
}

@After
public void suiteteardown()
{
driver.quit();	
}





}
