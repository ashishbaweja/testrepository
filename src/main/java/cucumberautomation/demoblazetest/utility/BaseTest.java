package cucumberautomation.demoblazetest.utility;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.io.Files;

public class BaseTest {
	
public WebDriver driver;
String Screenshotpath;
String url;
String username;
String password;
int ImplicitlywaitTime;
Properties prop;


public Properties ReadConfigData() throws IOException 
{
prop = new Properties();
FileInputStream fis = new FileInputStream(new File(System.getProperty("user.dir")+"\\src\\test\\java\\cucumberautomation\\demoblazetest\\configuration\\config.properties"));
prop.load(fis);
fis.close();	
return prop;
}	

public WebDriver setupBrowser() throws IOException 
{   prop = ReadConfigData();
    String driverpath=prop.getProperty("driverpath");			
    System.setProperty("webdriver.chrome.driver",driverpath);
	driver= new ChromeDriver();
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS)	;	
	driver.manage().window().maximize();		
	return driver;
}

	
public String Getscreenshot(WebDriver driver, String filename) throws IOException
{   
	java.util.Date date = Calendar.getInstance().getTime();  
	DateFormat dateFormat = new SimpleDateFormat("yy-mm-dd-hh-mm-ss");  
	String strDate = dateFormat.format(date);  
	Properties prop = ReadConfigData();
	Screenshotpath=System.getProperty("user.dir")+prop.getProperty("Screenshotpath");
	TakesScreenshot scn = ((TakesScreenshot)driver);
	File src = scn.getScreenshotAs(OutputType.FILE);
	File dest = new File(Screenshotpath +strDate+ filename +".png");
	Files.copy(src,dest);	
	return (Screenshotpath + filename +".png");
}

public void waituntilelementisvisible(WebDriver driver, WebElement elm, int timeout ) 
   {
	boolean staleElement = true; 
	while(staleElement){
	  try{
		   WebDriverWait wait = new WebDriverWait(driver, timeout);
	       wait.until(ExpectedConditions.visibilityOf(elm));
	       staleElement = false;
	  } catch(StaleElementReferenceException e){
	       staleElement = true;
	  }
	}
    
   }


}
