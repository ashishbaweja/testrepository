package cucumberautomation.demoblazetest.managers;

import org.openqa.selenium.WebDriver;


import cucumberautomation.demoblazetest.pageObjects.*;


public class PageObjectManager {
	private WebDriver driver;
	private HomePage homepage;
	private ProductPage productpage;
	private CartPage cartpage;
	private PlaceOrderPage placeorderpage;

	
	public PageObjectManager(WebDriver driver)
	{this.driver=driver;}
	
	
	public HomePage getHomePage()
	{
		return (homepage==null)? homepage=new HomePage(driver) : homepage;			
	}
	
	public ProductPage getProductPage()
	{
		return (productpage==null)? productpage=new ProductPage(driver) : productpage;			
	}
	
	public CartPage getCartPage()
	{
		return (cartpage==null)? cartpage=new CartPage(driver) : cartpage;			
	}
	
	public PlaceOrderPage getPlaceOrderPage()
	{
		return (placeorderpage==null)? placeorderpage=new PlaceOrderPage(driver) : placeorderpage;			
	}

}
