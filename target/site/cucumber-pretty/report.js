$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("DemoblazeE2E.feature");
formatter.feature({
  "line": 2,
  "name": "Test Demoblaze \"Add to cart\", \"Delete\", \"Place order\" and \"Purchase\" functionality.",
  "description": "Description: The Purpose of this test is to test Demoblaze online shop end to end functionality.",
  "id": "test-demoblaze-\"add-to-cart\",-\"delete\",-\"place-order\"-and-\"purchase\"-functionality.",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@demoblazetest"
    }
  ]
});
formatter.before({
  "duration": 7681719626,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Customer navigates through all product categories and purchases Laptop.",
  "description": "",
  "id": "test-demoblaze-\"add-to-cart\",-\"delete\",-\"place-order\"-and-\"purchase\"-functionality.;customer-navigates-through-all-product-categories-and-purchases-laptop.",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 6,
  "name": "user is on Demoblaze Online shop home page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user navigates through \"Phones\" category",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user navigates through \"Laptops\" category",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user navigates through \"Monitors\" category",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user navigates through \"Laptops\" category",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "adds \"Sony vaio i5\" to Cart",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user navigates through \"Laptops\" category",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "adds \"Dell i7 8gb\" to Cart",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "Deletes \"Dell i7 8gb\" from Cart",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "\"Dell i7 8gb\" should be deleted from Cart",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "user places order and fill required personal and fincancial details",
  "rows": [
    {
      "cells": [
        "Name",
        "Country",
        "City",
        "Credit Card",
        "Month",
        "Year"
      ],
      "line": 17
    },
    {
      "cells": [
        "Ashish",
        "India",
        "Gurugram",
        "6568*******",
        "12",
        "2022"
      ],
      "line": 18
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "proceeds to purchase products in cart",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "purchase amount should be \"790 USD\"",
  "keyword": "Then "
});
formatter.match({
  "location": "DemoblazeSteps.user_is_on_Demoblaze_Online_shop_home_page()"
});
formatter.result({
  "duration": 3673668054,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Phones",
      "offset": 24
    }
  ],
  "location": "DemoblazeSteps.user_navigates_through_category(String)"
});
formatter.result({
  "duration": 1042248442,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Laptops",
      "offset": 24
    }
  ],
  "location": "DemoblazeSteps.user_navigates_through_category(String)"
});
formatter.result({
  "duration": 2051511767,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Monitors",
      "offset": 24
    }
  ],
  "location": "DemoblazeSteps.user_navigates_through_category(String)"
});
formatter.result({
  "duration": 391517998,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Laptops",
      "offset": 24
    }
  ],
  "location": "DemoblazeSteps.user_navigates_through_category(String)"
});
formatter.result({
  "duration": 1678422677,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sony vaio i5",
      "offset": 6
    }
  ],
  "location": "DemoblazeSteps.adds_to_Cart(String)"
});
formatter.result({
  "duration": 6094755945,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Laptops",
      "offset": 24
    }
  ],
  "location": "DemoblazeSteps.user_navigates_through_category(String)"
});
formatter.result({
  "duration": 3281256527,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dell i7 8gb",
      "offset": 6
    }
  ],
  "location": "DemoblazeSteps.adds_to_Cart(String)"
});
formatter.result({
  "duration": 5118849964,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dell i7 8gb",
      "offset": 9
    }
  ],
  "location": "DemoblazeSteps.deletes_from_Cart(String)"
});
formatter.result({
  "duration": 7849055324,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dell i7 8gb",
      "offset": 1
    }
  ],
  "location": "DemoblazeSteps.should_be_deleted_from_Cart(String)"
});
formatter.result({
  "duration": 15055641992,
  "status": "passed"
});
formatter.match({
  "location": "DemoblazeSteps.user_places_order_and_fill_required_personal_and_fincancial_details(DataTable)"
});
formatter.result({
  "duration": 2279989431,
  "status": "passed"
});
formatter.match({
  "location": "DemoblazeSteps.proceeds_to_purchase_products_in_cart()"
});
formatter.result({
  "duration": 171448516,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "790 USD",
      "offset": 27
    }
  ],
  "location": "DemoblazeSteps.purchase_amount_should_be(String)"
});
formatter.result({
  "duration": 2235911979,
  "status": "passed"
});
formatter.after({
  "duration": 951437054,
  "status": "passed"
});
});